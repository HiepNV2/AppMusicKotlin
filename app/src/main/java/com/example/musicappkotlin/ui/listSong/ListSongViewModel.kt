package com.example.musicappkotlin.ui.listSong

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.musicappkotlin.model.Song
import com.example.musicappkotlin.ultil.AppRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListSongViewModel(private val appRepository: AppRepository) : ViewModel() {
    val picsData = MutableLiveData<ArrayList<Song>>()

    fun getListSong() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = appRepository.getMusic()
                picsData.postValue(response)
            } catch (e: Exception) {
                Log.getStackTraceString(e)
                picsData.postValue(ArrayList())
            }
        }

    }
}