package com.example.musicappkotlin.ui.listSong

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.musicappkotlin.ultil.ViewModelProviderFactory
import com.example.musicappkotlin.databinding.FragmentListSongBinding
import com.example.musicappkotlin.network.RetrofitService
import com.example.musicappkotlin.ultil.AppRepository

class ListSongFragment : Fragment() {
    private lateinit var binding: FragmentListSongBinding
    private lateinit var viewModel: ListSongViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        val retrofitService = RetrofitService.getInstance()
//        val repository = AppRepository(retrofitService)
//        val factory = ViewModelProviderFactory(repository)
        // Các thành phần khởi tạo như trên, viết thế này cho gọn
        val factory = ViewModelProviderFactory(AppRepository(RetrofitService.getInstance()))
        viewModel = ViewModelProvider(this, factory).get(ListSongViewModel::class.java)

        binding = FragmentListSongBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getListSong()

        viewModel.picsData.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.tvTest.text = it[0].musicUrl.toString()
                Log.d("AAA", it[0].musicUrl.toString())
            }
        })
    }
}