package com.example.musicappkotlin.ultil

import com.example.musicappkotlin.model.Song
import com.example.musicappkotlin.network.RetrofitService

class AppRepository(private val retrofitService: RetrofitService) {

    suspend fun getMusic(): ArrayList<Song> = retrofitService.getMusic()

}