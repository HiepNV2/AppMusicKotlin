package com.example.musicappkotlin.ultil

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.musicappkotlin.ui.listSong.ListSongViewModel

class ViewModelProviderFactory(
    private val appRepository: AppRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListSongViewModel::class.java)) {
            return ListSongViewModel(appRepository) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }

}