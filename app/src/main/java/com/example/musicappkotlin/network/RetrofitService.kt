package com.example.musicappkotlin.network

import com.example.musicappkotlin.model.Song
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetrofitService {

    @GET("/music")
    suspend fun getMusic(): ArrayList<Song>



    companion object {
        var retrofitService: RetrofitService? = null

        fun getInstance(): RetrofitService {
            if (retrofitService == null) {
                val client = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor())
                    .build()
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://6215b273c9c6ebd3ce2f03ca.mockapi.io")
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .client(client)
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}