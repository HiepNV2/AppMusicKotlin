package com.example.musicappkotlin.model

data class Music(
    val musicUrl: String,
    val thumail: String,
    val singer: String,
    val musicName: String,
    val id: String
)
